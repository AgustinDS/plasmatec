"""
Este código define funciones para transformar entre coordenadas cartesianas y toroidales,
calcular la transformada rotacional y para aplicar estas funciones a
directorios con datos resultantes de BS-SOLCTRA.
Autores: Johansell Villalobos, Esteban Pérez

Ingeniería, Física, TEC

Flujo de trabajo:
1- Se obtienen los datos de BS-Solctra en coodenadas cartesianas
2- Se transforman las coordenadas catesianas a cilindrico-toroidales usando
   la función "directorio_cart_a_tor()"
3- Se calcula la transformada rotacional de cada set de datos de trayectoria
   usando la función "calculo_iota()"
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import csv
import subprocess


def cart_to_tor(filename, R0, save_dir, main_dir):

	"""
	Entradas: filename -- str, R0 -- float.
	Salidas: "tor"+filename -- archivo .txt que contiene las coordenadas transformadas.
	Descrip: transfoma de coordenadas cartesianas a coordenads toroidales.
	"""
	#os.chdir(main_dir)
	# primero se cuenta el número de líneas del archivo para saber cuales hayq ue leer
	lineas = int(subprocess.check_output(['wc', '-l', filename]).split()[0])-2
	print('{}'.format(lineas))
	# part_path = np.loadtxt(filename, dtype=float,skiprows=1,delimiter='\t')

	part_path = np.loadtxt(filename, dtype=float, skiprows=1, delimiter=',', max_rows=lineas)

	#Cilindrical Toroidal Transform
	shp = part_path.shape
	#tor_coords = np.zeros(shp) #7 para los nuevos valores
	rotations = 0 
	os.chdir(save_dir)
	with open('tor'+filename,'w') as torfile:
		torfile.write("r,phi,theta,|B|,Bx,By,Bz\n")
		for i in range(shp[0]):
			xcart = part_path[i, 0]
			ycart = part_path[i, 1]
			zcart = part_path[i, 2]
			Bvalue = part_path[i, 3]
			Bx = part_path[i, 4]
			By = part_path[i, 5]
			Bz =part_path[i, 6]

			R = np.sqrt(xcart**2 + ycart**2)
			Rdiff = R - R0
			#Los ángulos están definidos de 0 a 2pi
			theta_tor = np.arctan2(zcart, Rdiff)
			if theta_tor < 0: 
				theta_tor = theta_tor+2*np.pi
			
			phi_tor = np.arctan2(ycart, xcart)
			if phi_tor < 0: 
				phi_tor = phi_tor+2*np.pi
			
			#comparación de posición entre cuadrantes para determinar cumplimiento de rotación
			
			if i != 0:
				xviejo = part_path[i-1,0]
				yviejo = part_path[i-1,1]
				posviejo = list(np.sign([xviejo, yviejo]))
				poscart = list(np.sign([xcart, ycart]))
				if posviejo == [1, -1] and poscart == [1, 1]: 
					rotations += 1 
					#print(rotations)
			
			#Como la rotación de las partículas es antihoraria se resta rotations*2*np.pi debido a que el ángulo es medido desde el eje y en sentido horario
			phi_tor = phi_tor + rotations*2*np.pi
			if zcart != 0: 
				r_tor = zcart/np.sin(theta_tor)
			else: 
				r_tor = abs(Rdiff)
				
			Br = Bx*np.cos(theta_tor)*np.sin(phi_tor)+By*np.cos(theta_tor)*np.cos(phi_tor)+Bz*np.sin(theta_tor)

			Btheta = Bx*np.sin(theta_tor)*np.sin(phi_tor)+By*np.sin(theta_tor)*np.cos(phi_tor)+Bz*np.cos(theta_tor)

			Bphi = Bx*np.cos(phi_tor)-By*np.sin(phi_tor)

			#tor_coords[i] = r_tor, phi_tor, theta_tor, Bvalue, Br, Btheta, Bphi
			torfile.write('{:e}\t{:e}\t{:e}\t{:e}\t{:e}\t{:e}\t{:e}\n'.format(r_tor, phi_tor, theta_tor, Bvalue, Br, Btheta, Bphi))
			#Agregar más entradas para el archivo de texto con 7 entradas
		torfile.close()
	os.chdir(main_dir)

	#return tor_coords


def tor_to_cart(filename,R0):
	"""
	Entradas: filename -- str, R0 -- float.
	Salidas: "tor"+filename -- archivo .txt que contiene las coordenadas transformadas.
	Descrip: transfoma de coordenadas toroidales a coordenadas cartesianas.
	"""

	part_path = np.loadtxt(filename, dtype=float,skiprows=1,delimiter='\t')

	#Transformar a coordenadas cartesianas
	shp = part_path.shape
	cart_coords = np.zeros(shp)

	with open('cart'+filename,'w') as cartfile:
		cartfile.write("x,y,z,|B|,Bx,By,Bz\n")
		for i in range(shp[0]):

			rtor = part_path[i, 0]
			phitor = part_path[i, 1]
			thetator = part_path[i, 2]
			Bvalue = part_path[i, 3]
			Bx = part_path[i, 4]
			By = part_path[i, 5]
			Bz =part_path[i, 6]

			x = (R0+rtor*np.cos(thetator))*np.sin(phitor)
			y = (R0+rtor*np.cos(thetator))*np.cos(phitor)
			z = rtor*np.sin(thetator)

			cart_coords[i] = x, y, z, Bvalue, Bx, By, Bz
			cartfile.write('{:e}\t{:e}\t{:e}\t{:e}\t{:e}\t{:e}\t{:e}\n'.format(x, y, z, Bvalue, Bx, By, Bz))

		cartfile.close()

	#return cart_coords


def directorio_cart_a_tor(work_dir, save_dir):
	"""
	Entradas: save_dir -- str; directorio para guardar el archivo.
			, work_dir -- str; directorio donde se encuentran todos los archivos de las
			  trayectorias
	Salidas: directorio con archivos en coordenadas toroidales.
	Descrip: transfoma de coordenadas cartesianas a coordenads toroidales; se guardan en "save_dir".
	"""
	print('Transformando archivos en {}'.format(work_dir))
	lista_archivos = os.listdir(work_dir)
	print(lista_archivos)
	lista_archivos.sort()
	print(lista_archivos)
	R_0 = 0.2477
	main_dir = os.getcwd()
	for iArch in lista_archivos:
		os.chdir(work_dir)
		print('\t-> Cambiando coordenadas de archivo: {}'.format(iArch))
		cart_to_tor(iArch, R_0, save_dir, main_dir)

	print('La transformación de coordenadas cartesianas a toroidales ha finalizado')


def errores_transf(file1, file2):
	"""
	Entradas: file1, file2 -- str, nombres de los archivos a comparar.
			file1: archivo control
			file2: archivo calculado con cart_to_tor() o con tor_to_cart()
	Salidas: error -- float, np.where -- list(np.arrays)
	Descrip: Encuentra el porcentaje del error más alto de file2 respecto a file1, también regresa la posición de ese error en el array de "diferencias"
	"""
	part_path1 = np.loadtxt(file1, dtype=float,skiprows=2,delimiter=',')
	part_path2 = np.loadtxt(file2, dtype=float,skiprows=2,delimiter='\t')

	diferencia = (part_path1 - part_path2)/part_path1
	error = np.max(abs(diferencia))


	print(error, np.where(abs(diferencia) == error))


def calculo_iota(ruta):
	"""
	Entradas: archivo -- string, ruta absoluta al directorio de datos de trayectoria
	Salidas: iota -- float, valor de iota para la trayectoria
	Descripción: Calcula el valor de iota para la trayectoria dada
	"""

	# Se importa el otro script donde está calculo_iota_4 de momento
	import transformada_Todoroki as todo

	lista_archivos = os.listdir(ruta)
	lista_archivos.sort()
	datos = []
	contador = 0

	with open('datos.txt','w') as data:
		for iArch in lista_archivos:
			iota, lineas = todo.Calculo_iota_4(ruta+iArch)
			datos.append([iArch, iota, lineas])

			data.write('{}\t{:.7f}\t{}\n'.format(datos[contador][0], datos[contador][1], datos[contador][2]))
			contador += 1

		data.close()

	# print('{}'.format(datos))


# def Calculo_iota_4(archivo_trayectoria):
#     """
#     Este método calcula la diferencia angular entre puntos separados por una revolución. Luego
#     se suman esas diferencias para cada revolución y se divide entre el número de revoluciones
#     archivo_trayectoria: es un archivo que contiene los datos de una trayectoria en
#     coordenadas toroidales
#     """
#     datos = np.loadtxt(archivo_trayectoria, skiprows = 1)
#
#     nPasos, _ = datos.shape
#     punto_inicial = np.zeros(3)
#     punto_final = np.zeros(3)
#
#     # Este contador indicará la revolución en la que se está
#     nRev = 1
#
#     # Variable a la que suman los delta_phi
#     sumatoria_delta_phi = 0
#
#     print('\n\n\nProcesando archivo {}'.format(archivo_trayectoria))
#
#     for iPaso in range(nPasos):
#         # si estamos en el primer paso se toma como punto inicial el primer punto
#         if iPaso == 0:
#             punto_inicial = datos[iPaso, :]
#
#
#         elif datos[iPaso, 1] < -2*np.pi*nRev:
#             # la condición original era para entrar a
#             # este if era datos[iPaso, 1] > 2*np.pi*nRev
#             # Se cambió por que las coordenadas toroidales
#             # aumentan en el sentido negativo
#
#             # print('Se completa la revolución {}'.format(nRev))
#             # Se almacena el primer punto que completa una revolución
#             punto_final = datos[iPaso, :]
#
#             # print('\tpunto inicial: {}\n\tpunto final: {}'.format(punto_inicial, punto_final))
#
#             # Se aumenta el contador
#             nRev += 1
#
#             # Se calcula la diferencia en los ángulos toroidales y se suma a la varible sumatoria
#             delta_phi = abs(punto_final[2] - punto_inicial[2])
#             sumatoria_delta_phi += delta_phi
#             # print('\tdiferencia angular poloidal: {}\n'.format(delta_phi))
#
#             # Se define al punto final como el nuevo inicial para continuar el ciclo
#             punto_inicial = np.array(punto_final)
#
#         # else:
#         #     continue
#
#     # Finalmente se divide la sumatoria de deltas entre el número de de rovoluciones
#     # para obtener el valor de la transformada rotacional
#     iota = sumatoria_delta_phi/(nRev*2*np.pi)
#
#     print('\tsumatoria: {:.7f}\tnRev: {}\tiota: {:.7f}\tlineas: {}'.format(sumatoria_delta_phi, nRev, iota, nPasos))
#
#     return iota, nPasos

# Las siguientes funciones tienen como objetivo graficar la transformada rotacional contra el radio de las superficies; aún están en depuración. 

def Calculo_iota_4(archivo_trayectoria, angulo):
	"""
	Este método calcula la diferencia angular entre puntos separados por una revolución. Luego
	se suman esas diferencias para cada revolución y se divide entre el número de revoluciones
	archivo_trayectoria: es un archivo que contiene los datos de una trayectoria en
	coordenadas toroidales
	"""
	datos = np.loadtxt(archivo_trayectoria, skiprows = 2)
	R0 = 0.2477
	nPasos, _ = datos.shape
	punto_inicial = np.zeros(3)
	punto_final = np.zeros(3)
	angulo = angulo*np.pi/180
	# Este contador indicará la revolución en la que se está
	nRev = 1

	# Variable a la que suman los delta_phi
	sumatoria_delta_phi = 0

	print('\n\nProcesando archivo {}'.format(archivo_trayectoria))

	rtor, _, _, _, _, _, _ = datos.T 
	R = np.max(rtor)+R0 #se utilizan los radios máximos para graficar, puede ser cambiada la definición 


	for iPaso in range(nPasos):
		# si estamos en el primer paso se toma como punto inicial el primer punto
		if iPaso == 0:
			punto_inicial = datos[iPaso, :]


		elif datos[iPaso,1]-datos[iPaso-1,1]<0:#isclose(datos[iPaso, 1],0,abs_tol=0.4e-2):
			# la condición original era para entrar a
			# este if era datos[iPaso, 1] > 2*np.pi*nRev
			# Se cambió por que las coordenadas toroidales
			# aumentan en el sentido negativo
			
			# print('Se completa la revolución {}'.format(nRev))
			# Se almacena el primer punto que completa una revolución
			punto_final = datos[iPaso, :]

			# print('\tpunto inicial: {}\n\tpunto final: {}'.format(punto_inicial, punto_final))

			# Se aumenta el contador
			nRev += 1

			# Se calcula la diferencia en los ángulos toroidales y se suma a la varible sumatoria
			delta_phi = abs(punto_final[2] - punto_inicial[2])
			if delta_phi < 2*np.pi-delta_phi:
				sumatoria_delta_phi += delta_phi
			else: 
				sumatoria_delta_phi += delta_phi
			
			# Se define al punto final como el nuevo inicial para continuar el ciclo
			punto_inicial = np.array(punto_final)
			
		else:
			continue

	# Finalmente se divide la sumatoria de deltas entre el número de de rovoluciones
	# para obtener el valor de la transformada rotacional
	iota = sumatoria_delta_phi/(nRev*2*np.pi)
	print('\tsumatoria: {:.7f}\tnRev: {}\tiota: {:.7f}\tlineas: {}\n\tRadio promedio: {}'.format(sumatoria_delta_phi, nRev, iota, nPasos, R))


	return iota, R

def grafico_iota(directorio, angulo): 

	print('Analizando archivos en {}'.format(directorio))
	lista_archivos = os.listdir(directorio)
	lista_archivos.sort()
	main_dir = os.getcwd()
	print("\tAccesando al directorio: "+directorio+"\n")

	iotas = list()
	radii = list()
	os.chdir(directorio)	
	for iArch in lista_archivos: 
		iota, rad= Calculo_iota_4(iArch, angulo)
		iotas.append(iota)
		radii.append(rad)
	
	vup = np.array([radii,iotas]).T
	print(vup)
	vup = vup[np.argsort(vup[:,0])]
	radii, iotas = vup.T
	os.chdir(main_dir)
	plt.plot(radii[0:len(radii)-1],iotas[0:len(iotas)-1], color="black") 
	plt.xlabel("R [m]")
	plt.ylabel("iota")
	plt.title(r"Transformada rotacional vs posición radial para $\theta = {}$".format(angulo))
	plt.savefig("iotas.png",dpi=300)
	plt.close()