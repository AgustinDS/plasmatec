import numpy as np
from matplotlib import pyplot as plt


def SuperficieDeContorno(datos):
    a = []
    b = []
    for i in range(len(datos)):
        if datos[i, 1] < 0:
            a.append(datos[i])
        else:
            b.append(datos[i])
    y1 = np.array(b)
    y2 = np.array(a)
    p1 = np.polyfit(y1[:, 0], y1[:, 1], 50)  # len(y1)
    p2 = np.polyfit(y2[:, 0], y2[:, 1], 50)  # len(y2)
    area1 = np.polyval(np.polyint(p1), np.max(y1[:, 0])) - np.polyval(np.polyint(p1), np.min(y1[:, 0]))
    area2 = -1*(np.polyval(np.polyint(p2), np.max(y2[:, 0])) - np.polyval(np.polyint(p2), np.min(y2[:, 0])))
    area = area1 + area2
    x = np.poly1d([1, 0])
    m = np.polymul(x, p1)
    n = np.polymul(x, p2)
    mint = np.polyval(np.polyint(m), np.max(y1[:, 0])) - np.polyval(np.polyint(m), np.min(y1[:, 0]))
    nint = -1*(np.polyval(np.polyint(n), np.max(y2[:, 0])) - np.polyval(np.polyint(n), np.min(y2[:, 0])))
    centroidex = (mint+nint)/area
    print("centroide = " + str(centroidex))
    print("área = " + str(area))


def cargar_datos(file_dir, angulo):
    angle_cut = angulo*np.pi/180
    dphi = 0.01
    phi_up = angle_cut + dphi/2
    phi_down = angle_cut - dphi/2
    R0 = 0.2477
    data = np.loadtxt(file_dir, dtype=float, skiprows=1, delimiter='\t')
    plotcoord = list()
    iRange, _ = data.shape
    fig, ax = plt.subplots(1, 1)
    for i in range(iRange):
        if data[i, 1] > phi_down and data[i, 1] < phi_up:
            rtor = data[i, 0]
            thetator = data[i, 2]
            R = (R0+rtor*np.cos(thetator))
            Z = rtor*np.sin(thetator)
            plotcoord.append([R, Z])
    datos = np.array(plotcoord)
    x = datos[:, 0]
    y = datos[:, 1]
    plt.scatter(x, y)
    plt.title(" Z vs R at phi=" + str(angulo))
    plt.show()
    plt.close()
    return datos

a = '/home/bnf/Escritorio/Repositorios/plasmatec/Cálculo de errores/resultstoroidal/torpath0029.txt'
b = cargar_datos(a, 1)
SuperficieDeContorno(b)
