# PlasmaTEC

Códigos del Laboratorio de Plasmas TEC

## Transformada

Contiene los scripts  que se han desarrollado para calcular la transformada rotacional con los datos de BS-SOLCTRA. Colaboradores: Esteban y Johansell

## Sonda-de-Langmuir

Archivos relacionados a cálculos con la sonda de Langmuir. Colaboradores: Ricardo y Allan.

## Cálculos de errores

Colaboradores: Ricardo, Bryam y Allan

## Diagnósticos magnéticos

Colaboradores: Arnoldo